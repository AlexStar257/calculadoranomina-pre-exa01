package com.example.calculadoranomina;

import java.text.DecimalFormat;

public class ReciboNomina {
    private int numRecibo;
    private String nombre;
    private float horasNormal;
    private float horasExtras;
    private int puesto;
    private float impuesto;

    public ReciboNomina(int numRecibo, String nombre, float horasNormal, float horasExtras, int puesto, float impuesto) {
        this.numRecibo = numRecibo;
        this.nombre = nombre;
        this.horasNormal = horasNormal;
        this.horasExtras = horasExtras;
        this.puesto = puesto;
        this.impuesto = impuesto;
    }

    // Getters y Setters
    public int getNumRecibo() {
        return numRecibo;
    }

    public void setNumRecibo(int numRecibo) {
        this.numRecibo = numRecibo;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public float getHorasTrabNormal() {
        return horasNormal;
    }

    public void setHorasTrabNormal(float horasTrabNormal) {
        this.horasNormal = horasTrabNormal;
    }

    public float getHorasTrabExtras() {
        return horasExtras;
    }

    public void setHorasTrabExtras(float horasTrabExtras) {
        this.horasExtras = horasTrabExtras;
    }

    public int getPuesto() {
        return puesto;
    }

    public void setPuesto(int puesto) {
        this.puesto = puesto;
    }

    public float getImpuestoPorc() {
        return impuesto;
    }

    public void setImpuestoPorc(float impuestoPorc) {
        this.impuesto = impuestoPorc;
    }

    // Métodos para cálculos

    public float reciboNominal() {
        float pagoBase = 200f;
        switch (puesto) {
            case 1:
                return pagoBase * 1.2f;
            case 2:
                return pagoBase * 1.5f;
            case 3:
                return pagoBase * 2.0f;
            default:
                return pagoBase;
        }
    }
    public float calcularSubtotal() {
        float pagoBase = reciboNominal();
        float pagoHoraExtra = pagoBase * 2;
        float subtotal = (pagoBase * horasNormal) + (pagoHoraExtra * horasExtras);
        return formatFloat(subtotal);
    }
    public float calcularImpuesto() {
        float subtotal = calcularSubtotal();
        float impues = subtotal * (impuesto / 100);
        return formatFloat(impues);
    }
    public float calcularTotal() {
        float subtotal = calcularSubtotal();
        float impuesto = calcularImpuesto();
        float total = subtotal - impuesto;
        return formatFloat(total);
    }
    private float formatFloat(float value) {
        DecimalFormat decimalFormat = new DecimalFormat("#.00");
        return Float.parseFloat(decimalFormat.format(value));
    }
}


